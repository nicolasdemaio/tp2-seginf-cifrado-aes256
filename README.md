# TP2 SegInf Cifrado AES256

## Proyecto

### Grupo
- De Maio, Nicolas
- Espinola, Abel
- Jara, Julieta
- Ruiz Marquez, Gonzalo Osvaldo


### Objetivo
> Realizar un programa que tome por entrada un archivo cualquiera y devuelva el mismo archivo cifrado con el algoritmo _**AES-256**_.
Se podrá utilizar el lenguaje de programación preferido por el grupo.
Deberá entregarse un breve informe explicando el funcionamiento del código.
El informe deberá contener un anexo explicando cómo se deberá efectuar la
ejecución.


### Documentación
[Enlace a google Docs](https://docs.google.com/document/d/1kYK7l5dTnHExnzywsLCC2qxEypvRW3cPgb1vSF4XJ-8/edit?hl=es)