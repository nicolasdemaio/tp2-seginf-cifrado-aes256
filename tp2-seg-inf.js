/* Imports */
const fs = require('fs');
const crypto = require('crypto');
const readline = require('readline');

/* Constants. */
const AES_MODE = 'aes-256-cbc';
const UNICODE_FORMAT = 'utf-8';

/* Readline to read inputs of users. */
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

/* Notifies the user that encryption was successful. */
const logSuccessfulOperation = (outputFileName) => {
  console.log('El archivo ha sido cifrado con éxito.');
  console.log('Archivo de salida: ' + outputFileName);
  console.log('Programa finalizado.');
}

/* Notifies the user that an error has occurred. */
const logErrorOperation = (error) => {
  console.error('Ocurrio un error:', error.message);
  console.log('Programa finalizado.');
}

/* Generates output file name adding suffix '_encrypted' to input file name. */
const generateOutputFileName = (inputFileName) => {
  const fileName = inputFileName.split('.')[0];
  return fileName + '_encrypted.txt';
}

/* */
const ensureFileExists = (inputFileName) => {
  if (!fs.existsSync(inputFileName)) {
    throw new Error(`El archivo '${inputFileName}' no existe.`);
  }
}

/* Entry point. */
console.log('Programa inicializado.');
rl.question('Ingresá el nombre del archivo a cifrar: ', (inputFileName) => {
  rl.question('Clave de cifrado (debe ser de 256 bits): ', (keyInput) => {
    try {
      const inputFileNameText = inputFileName.trim();
      const outputFileName = generateOutputFileName(inputFileNameText);

      const key = Buffer.from(keyInput, UNICODE_FORMAT);

      // Random 16-byte (128-bit) secure initialization vector (iv).
      const iv = crypto.randomBytes(16);

      ensureFileExists(inputFileNameText);
      const fileToEncrypt = fs.createReadStream(inputFileNameText);

      const outputFile = fs.createWriteStream(outputFileName);

      // Creates an AES cipher object using CBC mode.
      const cipher = crypto.createCipheriv(AES_MODE, key, iv);

      fileToEncrypt.pipe(cipher).pipe(outputFile);

      outputFile.on('finish', () => {
        logSuccessfulOperation(outputFileName);
        rl.close();
      });
    } catch (error) {
      logErrorOperation(error)
      rl.close();
    }
  });
});

